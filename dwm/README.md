# dwm - dynamic window manager
dwm is an extremely fast, small, and dynamic window manager for X.

Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
enter the following commands to build and install dwm:

    git clone https://github.com/RedsonBr140/dwm
    cd dwm
    sudo make install
    
Patches
-------
It's my personal build of dwm, with the following patches:
 - [fullgaps](https://dwm.suckless.org/patches/fullgaps/): Gaps in all windows
 - [Hide vacant tags](https://dwm.suckless.org/patches/hide_vacant_tags/): Hide unused workspaces from dwm's bar
 - [Resizecorners](https://dwm.suckless.org/patches/resizecorners/): By default, dwm can only resize floating windows on the inferior right corner, this patch allow to resize in all window corners
- [colorbar](https://dwm.suckless.org/patches/colorbar/): With this patch you can change the color of any statusbar element.
- [status2d](https://dwm.suckless.org/patches/status2d/): With this patch you can change the color of the bar elements, or draw things.

//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"^c#fdf6e3^",   "~/dev/suckless/dwmblocks/scripts/battery.sh",				5,		0},
	{"^c#268bd2^ ",   "echo  `pacmd list | grep 'active port' | awk '{print $4}' | sed -n '1p' | cut -c 1-2`: `pactl get-sink-volume 0 | awk '{print $5}' | sed -n '1p'`",					5,		0},
	{"^c#dc322f^ ", "sensors | grep 'Core 0' | awk '{print $3}' | sed 's/+//'",	1,		0},
	{"^c#b58900^﬙ ", "free -h | awk '/^Mem/ { print $3}' | sed s/i//g",	1,		0},
	{"^c#839496^ ", "date '+%I:%M'",									1,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 5;

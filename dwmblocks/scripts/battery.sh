memando=$(echo "$(cat /sys/class/power_supply/axp288_fuel_gauge/capacity)")

if [ "$memando" -ge "90" ]; then
	echo "   ${memando}%"
fi
if [ "$memando" -ge "50" ]; then
	echo "   ${memando}%"
fi
if [ "$memando" -ge "35" ]; then
	echo "   ${memando}%"
fi
if [ "$memando" -ge "25" ]; then
	echo "   ${memando}%"
fi
if [ "$memando" -ge "0" ]; then
	echo "   ${memando}%"
fi

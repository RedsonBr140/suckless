st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib and Harfbuzz header files.


Installation
------------
enter the following commands to build and install st:

    git clone https://gitlab.com/RedsonBr140/suckless
    cd suckless/st
    sudo make clean install

Patches
-------
It's my personal build of st, with the following patches:
 - [solarized dark](https://st.suckless.org/patches/solarized/): These patche make the Solarized color scheme available for st.
 - [blinking cursor](https://st.suckless.org/patches/blinking_cursor/): This patch allows the use of a blinking cursor.
 - [font2](https://st.suckless.org/patches/font2/): Allow you to have more than one font loaded into st.
 - [no bold colors](https://st.suckless.org/patches/solarized/st-no_bold_colors-20170623-b331da5.diff) : Part of the solarized-dark patch.
 - [scrollback](https://st.suckless.org/patches/scrollback/): Patch that allows scrolling with the mouse.

Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

